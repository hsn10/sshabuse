import java.sql.DriverManager

object Main extends App {

   val JDBC_URL = "jdbc:derby:directory:testdb";

   createDerby()
   connectDerby()
   shutdownDerby()
   
   def connectDerby() = {
      new org.apache.derby.jdbc.EmbeddedDriver()
      DriverManager.getConnection(JDBC_URL)
   }

   def createDerby() = {
      import org.flywaydb.core.Flyway
      new org.apache.derby.jdbc.EmbeddedDriver()
      System.setProperty("derby.storage.pageSize","8192")
      Flyway.configure().dataSource(s"$JDBC_URL;create=true", null, null).load().migrate();
   }

   def shutdownDerby() = {
      import java.sql.SQLException
      try {
         DriverManager.getConnection("jdbc:derby:;shutdown=true")
      }
      catch {
         case e: SQLException if (e.getErrorCode() == 50000) =>
      }
   }
}