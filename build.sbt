
startYear := Some(2020)

scalaVersion := "2.13.2"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.1.1" % "test"

libraryDependencies += "org.apache.derby" % "derby" % "10.14.2.0"

libraryDependencies += "org.flywaydb" % "flyway-core" % "6.4.1"

fork := true